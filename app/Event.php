<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'date_inicio','date_fim', 'image_destaque',
    ];

    public function servicos(){

        return $this->belongsToMany(Servico::class, 'event_servicos', 'event_id', 'servico_id')->withPivot('id');
    }
}
