<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');


Route::get('/teste', function () {
    return view('welcome');
});
Route::group(['prefix'=> 'api'], function(){
    Route::group(['as'=> 'user.' ,'prefix'=> 'users' ], function(){

        Route::get('', ['as'=> 'index', 'uses'=> 'UserController@index']);
        Route::post('salvar', ['as'=> 'store', 'uses'=> 'UserController@store']);
        Route::post('end', ['as'=> 'end', 'uses'=> 'UserController@ende']);
        Route::get('{id}/editar', ['as'=> 'edit', 'uses'=> 'UserController@edit']);
        Route::post('{id}/atualizar', ['as'=>'update', 'uses'=> 'UserController@update']);
        Route::get('{id}/remover', ['as'=> 'destroy', 'uses'=> 'UserController@destroy']);
        Route::get('{id}/ver', ['as'=> 'destroy',  'uses'=> 'UserController@show']);
        Route::get('{email}/endereco', ['as'=> 'endereco', 'uses'=> 'UserController@endereco']);
        Route::get('{email}/buscar-dados', ['as'=> 'buscarDados', 'uses'=> 'UserController@buscarDados']);
        Route::get('{id}/user-endereco', ['as'=> 'trazer', 'uses'=> 'UserController@trazerUmEndereco']);
        Route::post('{id}/salve-pedido', ['as'=>'salvePedido', 'uses'=> 'UserController@salvePedido']);
        Route::post('{id}/pedidos', ['as'=>'pedidos', 'uses'=> 'UserController@pedidos']);
        Route::post('{id}/avalidadores', ['as'=>'avalidadores', 'uses'=> 'UserController@avalidadores']);
        Route::get('{id}/colaboradores', ['as'=>'colaboradores', 'uses'=> 'UserController@colaboradores']);
        Route::post('{id}/remover-cargo', ['as'=>'removerCargo', 'uses'=> 'UserController@removerCargo']);
        Route::post('dados', ['as'=>'dados', 'uses'=> 'UserController@dados']);
    });

    Route::group(['as'=> 'event.' ,'prefix'=> 'events' ], function(){

        Route::get('', ['as'=> 'index', 'uses'=> 'EventController@index']);
        Route::post('salvar', ['as'=> 'store', 'uses'=> 'EventController@store']);
        Route::post('end', ['as'=> 'end', 'uses'=> 'EventController@ende']);
        Route::get('{id}/editar', ['as'=> 'edit', 'uses'=> 'EventController@edit']);
        Route::post('{id}/atualizar', ['as'=>'update', 'uses'=> 'EventController@update']);
        Route::get('{id}/remover', ['as'=> 'destroy', 'uses'=> 'EventController@destroy']);
        Route::get('{id}/ver', ['as'=> 'destroy',  'uses'=> 'EventController@show']);

        Route::post('dados', ['as'=>'dados', 'uses'=> 'UserController@dados']);
    });
});
