<?php
/**
 * Created by PhpStorm.
 * User: thiago
 * Date: 04/10/16
 * Time: 16:35
 */

namespace App\Repositories;
use App\User;


class UserRepository extends  AbstractRepository
{
    protected $model;

    public function __construct(User $model){
        $this->model = $model;
    }
}