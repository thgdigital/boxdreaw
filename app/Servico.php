<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'icone','image_destaque',
    ];

    public function events(){
        return $this->belongsToMany(Event::class, 'event_servicos', 'servico_id', 'event_id')->withPivot('id');
    }
}