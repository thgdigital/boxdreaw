<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
//
//$factory->define(App\User::class, function (Faker\Generator $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->safeEmail,
//        'password' => bcrypt(str_random(10)),
//        'remember_token' => str_random(10),
//    ];
//});
//$factory->define(App\Servico::class, function (Faker\Generator $faker){
//    return [
//        'name' => $faker->word(),
//        'description' =>  $faker->text($maxNbChars = 200),
//        'icone' => "teste",
//        'image_destaque' => $faker->imageUrl(640, 480, 'nightlife')
//    ];
//});
$factory->define(App\Event::class, function (Faker\Generator $faker){
    return [
        'name' => $faker->word(),
        'description' =>  $faker->text($maxNbChars = 200),
        'date_inicio' =>  $faker->dateTimeThisMonth($max = 'now'),
        'date_fim' =>  $faker->dateTimeThisMonth($max = 'now'),
        'image_destaque' => $faker->imageUrl(640, 480, 'nightlife')
    ];
});